library ieee;
use ieee.std_logic_1164.all;

entity Zegarek is
	port(SW :in std_logic_vector(10 downto 0);
		  KEY :in std_logic_vector(2 downto 0);
		  CLOCK_50 :in std_logic;
		  LEDG: out std_logic_vector(7 downto 0);
		  LEDR: out std_logic_vector(15 downto 0);
		  HEX0: out std_logic_vector(6 downto 0);
		  HEX1: out std_logic_vector(6 downto 0);
		  HEX2: out std_logic_vector(6 downto 0);
		  HEX3: out std_logic_vector(6 downto 0);
		  HEX4: out std_logic_vector(6 downto 0);
		  HEX5: out std_logic_vector(6 downto 0);
		  HEX6: out std_logic_vector(6 downto 0));
end Zegarek;

architecture arch_zegarek of Zegarek is
	component base_slow is 
		port (frequency 	: in std_logic;
				frequency_o : out std_logic);			
	end component;
	component minsec_slow is 
		port (frequency, set : in std_logic;
				setter 		: in std_logic_vector (7 downto 0);
				count			: out std_logic_vector(7 downto 0);
				frequency_o : out std_logic);			
	end component;
	component hours_slow is
		port (frequency, set : in std_logic;
				setter		: in std_logic_vector(7 downto 0);
				count			: out std_logic_vector(7 downto 0);
				frequency_o : out std_logic);			
	end component;
	component BCDtoBIN is
		port(BCD_wej: in std_logic_vector(7 downto 0);
			  BIN_wyj: out std_logic_vector(7 downto 0));
	end component;
	component BuzzMemory is
		port(Wej_H: in std_logic_vector(7 downto 0);
			  Wej_Min: in std_logic_vector(7 downto 0);
			  Set_Buzzer_H: in std_logic;
			  Set_Buzzer_Min: in std_logic;
			  Wyj_H: out std_logic_vector(7 downto 0);
			  Wyj_Min: out std_logic_vector(7 downto 0));
	end component;
	component compare_time is
		port(Buzz_Min: in std_logic_vector(7 downto 0);
			  Buzz_H: in std_logic_vector(7 downto 0);
			  Clock_Min: in std_logic_vector(7 downto 0);
			  Clock_H: in std_logic_vector(7 downto 0);
			  Bzzz: out std_logic);
	end component;
	component Miganie is
		port(sekunda: in std_logic;
			  ifAlarm: in std_logic;
			  MigMig: out std_logic_vector (7 downto 0));
	end component;
	component BINtoBCD is
		port(BIN_wej: in std_logic_vector(7 downto 0);
			  BCD_wyj: out std_logic_vector(7 downto 0));
	end component;
	component h24to12 is
		port(Wej: in std_logic_vector(7 downto 0);
			  Wyj: out std_logic_vector(7 downto 0));
	end component;
	component compare12 is 		
		port( W	:in std_logic_vector(7 downto 0);
				S	: out std_logic_vector(1 downto 0));
	end component;
	component mux2_bus2 is
		port(A, B: in std_logic_vector(1 downto 0);
				S   : in std_logic;
				W	: out std_logic_vector(1 downto 0));
	end component;
	component PA_12 is
		port(Wej_mux: in std_logic_vector(1 downto 0);
			  Wyj_HEX: out std_logic_vector(6 downto 0));
	end component;
	component mux2_bus8 is
		port(A, B: in std_logic_vector(7 downto 0);
			  S   : in std_logic;
			  W	: out std_logic_vector(7 downto 0));
	end component;
signal second: std_logic;
signal minute: std_logic;
signal hour: std_logic;
signal BIN_output: std_logic_vector(7 downto 0);
signal seconds_out: std_logic_vector(7 downto 0);
signal minutes_out: std_logic_vector(7 downto 0);
signal hours_out: std_logic_vector(7 downto 0);
signal Buzz_hours: std_logic_vector(7 downto 0);
signal Buzz_minutes: std_logic_vector(7 downto 0);
signal h_mode: std_logic_vector(7 downto 0);
signal alarm: std_logic;
signal P_A: std_logic_vector(1 downto 0);
signal PA_Wyj: std_logic_vector(1 downto 0);
signal disp_h: std_logic_vector(7 downto 0);
	begin
M_base_slower: base_slow port map(frequency => CLOCK_50, frequency_o => second);
A_to_BIN: BCDtoBIN port map(BCD_wej(0) => SW(0), BCD_wej(1) => SW(1), BCD_wej(2) => SW(2), BCD_wej(3) => SW(3), BCD_wej(4) => SW(4), BCD_wej(5) => SW(5), BCD_wej(6) => SW(6), BCD_wej(7) => SW(7), BIN_wyj => BIN_output);
R_seconds_slower: minsec_slow port map(frequency => second, set => ((not KEY(2)) AND (not SW(8))), setter => BIN_output, count => seconds_out, frequency_o => minute);
E_minutes_slower: minsec_slow port map(frequency => minute, set => ((not KEY(1)) AND (not SW(8))), setter => BIN_output, count => minutes_out, frequency_o => hour);
C_hours_slower: hours_slow port map(frequency => hour, set => ((not KEY(0)) AND (not SW(8))), setter => BIN_output, count => hours_out, frequency_o => open);
Z_MEM_buzz: BuzzMemory port map(Wej_H => BIN_output, Wej_Min => BIN_output, Set_Buzzer_H => ((not KEY(0)) AND SW(8)), Set_Buzzer_Min => ((not KEY(1)) AND SW(8)), Wyj_H => Buzz_hours, Wyj_Min => Buzz_minutes);
E_time_compare: compare_time port map(Buzz_Min => Buzz_minutes, Buzz_H => Buzz_hours, Clock_Min => minutes_out, Clock_H => hours_out, Bzzz => alarm);
K_alarm_ledg: Miganie port map(sekunda => second, ifAlarm => (alarm AND (not SW(10))), MigMig => LEDG);

M_to_BCD_J: BINtoBCD port map(BIN_wej => Buzz_minutes, BCD_Wyj(0) => LEDR(0), BCD_Wyj(1) => LEDR(1), BCD_Wyj(2) => LEDR(2), BCD_Wyj(3) => LEDR(3), BCD_Wyj(4) => LEDR(4), BCD_Wyj(5) => LEDR(5), BCD_Wyj(6) => LEDR(6), BCD_Wyj(7) => LEDR(7));
I_to_BCD_D: BINtoBCD port map(BIN_wej => Buzz_hours, BCD_Wyj(0) => LEDR(8), BCD_Wyj(1) => LEDR(9), BCD_Wyj(2) => LEDR(10), BCD_Wyj(3) => LEDR(11), BCD_Wyj(4) => LEDR(12), BCD_Wyj(5) => LEDR(13), BCD_Wyj(6) => LEDR(14), BCD_Wyj(7) => LEDR(15));
S_mode_12_24: h24to12 port map(Wej => hours_out, Wyj => h_mode);
I_mode_PA: compare12 port map(W => hours_out, S => P_A);
A_mux_PA: mux2_bus2 port map(A => "00", B => P_A, S => SW(9), W => PA_Wyj);
C_disp_PA: PA_12 port map(Wej_mux => PA_Wyj, Wyj_HEX => HEX6);
Z_disp_h_mode: mux2_bus8 port map(A => hours_out, B => h_mode, S => SW(9), W => disp_h);
K_
U_
end arch_zegarek;

library ieee;
use ieee.std_logic_1164.all;

entity PA_12 is
	port(Wej_mux: in std_logic_vector(1 downto 0);
		  Wyj_HEX: out std_logic_vector(6 downto 0));
end PA_12;

architecture arch_PA_12 of PA_12 is
signal wejscie: std_logic_vector(1 downto 0);
signal wyjscie: std_logic_vector(6 downto 0);
begin
	wejscie <= Wej_mux;
	with wejscie select
		wyjscie <= "0001000" when "01",
					  "0001100" when "10",
					  "1111111" when others;
	Wyj_HEX <= wyjscie;
end arch_PA_12;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BuzzMemory is
	port(Wej_H: in std_logic_vector(7 downto 0);
		  Wej_Min: in std_logic_vector(7 downto 0);
		  Set_Buzzer_H: in std_logic;
		  Set_Buzzer_Min: in std_logic;
		  Wyj_H: out std_logic_vector(7 downto 0);
		  Wyj_Min: out std_logic_vector(7 downto 0));
end BuzzMemory;

architecture arch_BuzzMemory of BuzzMemory is
signal Memory_H: std_logic_vector(7 downto 0);
signal Memory_Min: std_logic_vector(7 downto 0);
begin
	process(Set_Buzzer_H)
	begin
		if (Set_Buzzer_H = '1') then
			Memory_H <= std_logic_vector(unsigned(Wej_H) mod 24);
		end if;
	end process;
	process(Set_Buzzer_Min)
	begin
		if (Set_Buzzer_Min = '1') then
			Memory_Min <= std_logic_vector(unsigned(Wej_Min) mod 60);
		end if;
	end process;
	Wyj_H <= Memory_H;
	Wyj_Min <= Memory_Min;
end arch_BuzzMemory;

library ieee;
use ieee.std_logic_1164.all;

entity compare_time is
	port(Buzz_Min: in std_logic_vector(7 downto 0);
		  Buzz_H: in std_logic_vector(7 downto 0);
		  Clock_Min: in std_logic_vector(7 downto 0);
		  Clock_H: in std_logic_vector(7 downto 0);
		  Bzzz: out std_logic);
end compare_time;

architecture arch_compare_time of compare_time is
begin
	process(Buzz_Min,Buzz_H,Clock_H,Clock_Min)
	begin
		if ((Buzz_Min = Clock_Min) AND (Buzz_H = Clock_H)) then
			Bzzz <= '1';
		else
			Bzzz <= '0';
		end if;
	end process;
end arch_compare_time;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BCDtoBIN is
	port(BCD_wej: in std_logic_vector(7 downto 0);
		  BIN_wyj: out std_logic_vector(7 downto 0));
end BCDtoBIN;

architecture arch_BCDtoBIN of BCDtoBIN is
signal BCD: std_logic_vector(7 downto 0);
signal BIN_J: unsigned(7 downto 0);
signal BIN_D: unsigned(7 downto 0);
signal BIN_Wynik: unsigned(7 downto 0);
signal mergeJ: std_logic_vector(7 downto 0);
signal mergeD: std_logic_vector(7 downto 0);
	begin
		BCD <= BCD_wej;
		process(BCD)
		begin
			mergeJ <= ('0' & '0' & '0' & '0' & BCD(3) & BCD(2) & BCD(1) & BCD(0));
			mergeD <= ('0' & '0' & '0' & '0' & BCD(7) & BCD(6) & BCD(5) & BCD(4));
			BIN_J <= unsigned(mergeJ);
			BIN_D <= unsigned(mergeD);
			BIN_Wynik <= BIN_J + BIN_D * 10;
		end process;
		BIN_wyj <= std_logic_vector(BIN_Wynik);
end arch_BCDtoBIN;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BINtoBCD is
	port(BIN_wej: in std_logic_vector(7 downto 0);
		  BCD_wyj: out std_logic_vector(7 downto 0));
end BINtoBCD;

architecture arch_BINtoBCD of BINtoBCD is
signal BIN_unsig: unsigned (7 downto 0);
signal BCD_J: unsigned (7 downto 0);
signal BCD_D: unsigned (7 downto 0);
begin
	BIN_unsig <= unsigned(BIN_wej);
	BCD_J <= BIN_unsig mod 10;	
	BCD_D <= BIN_unsig / 10;
	BCD_wyj <= (std_logic_vector(BCD_D(3 downto 0)) & std_logic_vector(BCD_J(3 downto 0)));
end arch_BINtoBCD;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity compare12 is 		
	port( W	:in std_logic_vector(7 downto 0);
			S	: out std_logic_vector(1 downto 0));
end compare12;

architecture arch_compare12 of compare12 is
	signal Wej: unsigned(7 downto 0);
	signal Wyj: std_logic_vector(1 downto 0);
begin
	Wej <= unsigned(W);
	process(Wej)
	begin
		if (Wej > "00001011") then
			Wyj <= "10";
		else
			Wyj <= "01";
		end if;
	end process;
	
	S <= Wyj;
end arch_compare12;

library ieee;
use ieee.std_logic_1164.all;

entity mux2_bus8 is
	port(A, B: in std_logic_vector(7 downto 0);
		  S   : in std_logic;
		  W	: out std_logic_vector(7 downto 0));
end mux2_bus8;

architecture arch_mux2bus8 of mux2_bus8 is
begin 
	with S select
		W <= A when '0',
			  B when '1';
end arch_mux2bus8;

library ieee;
use ieee.std_logic_1164.all;

entity mux2_bus2 is
	port(A, B: in std_logic_vector(1 downto 0);
		  S   : in std_logic;
		  W	: out std_logic_vector(1 downto 0));
end mux2_bus2;

architecture arch_mux2bus2 of mux2_bus2 is
begin 
	with S select
		W <= A when '0',
			  B when '1';
end arch_mux2bus2;
		
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity hours_slow is
	port (frequency, set : in std_logic;
			setter		: in std_logic_vector(7 downto 0);
			count			: out std_logic_vector(7 downto 0);
			frequency_o : out std_logic);			
end hours_slow;

architecture arch_hours of hours_slow is
	constant LICZNIK_LIMIT : integer := 24;
	signal licznik : unsigned(7 downto 0);	
begin
count <= std_logic_vector(licznik);
	process(frequency)
		begin
			if set = '1' then
				licznik <= unsigned(unsigned(setter) mod 24);
			end if;
			
			if rising_edge(frequency) then
				if licznik = LICZNIK_LIMIT then
					licznik <= (others => '0');
					frequency_o <= '1';
				else 
					licznik <= licznik + 1;
					frequency_o <= '0';
				end if;
			end if;
	end process;
end arch_hours;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity minsec_slow is 
	port (frequency, set : in std_logic;
			setter 		: in std_logic_vector (7 downto 0);
			count			: out std_logic_vector(7 downto 0);
			frequency_o : out std_logic);			
end minsec_slow;

architecture arch_minsec of minsec_slow is
	constant LICZNIK_LIMIT : integer := 60;
	signal licznik : unsigned(7 downto 0);
begin
count <= std_logic_vector(licznik);
	process(frequency)
		begin
			if set = '1' then
				licznik <= unsigned(unsigned(setter) mod 60);
			end if;
			
			if rising_edge(frequency) then
				if licznik = LICZNIK_LIMIT then
					licznik <= (others => '0');
					frequency_o <= '1';
				else 
					licznik <= licznik + 1;
					frequency_o <= '0';
				end if;
			end if;
	end process;
end arch_minsec;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity base_slow is 
	port (frequency 	: in std_logic;
			frequency_o : out std_logic);			
end base_slow;

architecture arch_base of base_slow is
	constant LICZNIK_LIMIT : integer := 50000000;
	signal licznik : unsigned(25 downto 0);
begin
	process(frequency)
		begin
			if rising_edge(frequency) then
		
				if licznik = LICZNIK_LIMIT then
					licznik <= (others => '0');
					frequency_o <= '1';
				else 
					licznik <= licznik + 1;
					frequency_o <= '0';
				end if;
			end if;
	end process;
end arch_base;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity h24to12 is
	port(Wej: in std_logic_vector(7 downto 0);
		  Wyj: out std_logic_vector(7 downto 0));
end h24to12;

architecture arch_24to12 of h24to12 is
signal cosiek: std_logic_vector(7 downto 0);
begin
	cosiek <= std_logic_vector(unsigned(Wej) mod 12);
	process(Wej)
	begin
		if cosiek = "00000000" then
			Wyj <= "00001100";
		else
			Wyj <= cosiek;
		end if;
	end process;
end arch_24to12;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity seg7 is
	port( MSH: in std_logic_vector(7 downto 0);
			Wys_J: out std_logic_vector(6 downto 0);
			Wys_D: out std_logic_vector(6 downto 0));
end seg7;

architecture arch_seg7 of seg7 is
	signal wej: unsigned(7 downto 0);
	signal Jednosci: unsigned(7 downto 0);
	signal Dziesiatki: unsigned(7 downto 0);
begin
	wej <= unsigned(MSH);
	Jednosci <= wej mod 10;
	Dziesiatki <= wej /10;
	with Jednosci select
		Wys_J <= "1000000" when "00000000",
					"1111001" when "00000001",
					"0100100" when "00000010",
					"0110000" when "00000011",
					"0011001" when "00000100",
					"0010010" when "00000101",
					"0000010" when "00000110",
					"1111000" when "00000111",
					"0000000" when "00001000",
					"0010000" when others;
	with Dziesiatki select
		Wys_D <= "1000000" when "00000000",
					"1111001" when "00000001",
					"0100100" when "00000010",
					"0110000" when "00000011",
					"0011001" when "00000100",
					"0010010" when "00000101",
					"1111111" when others;
			  
end arch_seg7;	

library ieee;
use ieee.std_logic_1164.all;

entity Miganie is
	port(sekunda: in std_logic;
		  ifAlarm: in std_logic;
		  MigMig: out std_logic_vector (7 downto 0));
end Miganie;

architecture arch_Miganie of Miganie is
signal Animacja: std_logic_vector(7 downto 0) :="00000000";
begin
	MigMig <= Animacja;
	process(sekunda)
	begin
		if (ifAlarm = '1') then
			Animacja <= not Animacja;
		else
			Animacja <= "00000000";
		end if;
	end process;
end arch_Miganie;